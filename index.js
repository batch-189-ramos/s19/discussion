// alert("Hello World");

/*selection Control Structures
	- it sorts out whether the stament/s are to be executed based on the condition whether it is true or false.
ex:
	if ... else statement

	Syntax:
		if(condition) {
			//statement
		}	else {
			//statement
		}

	if statement - Executes a statement if a specified condition is true

	Syntax:
		if(condition) {
			//statement
		}

*/


let numA = -1;

if(numA < 0) {
	console.log("Hello");
}

console.log(numA < 0);

let city = "New York";

if(city === "New York") {
	console.log("Welcome to New York City!");
}


/*
	Else if
		- executes a statement if our previous conditions are false and if the specified conditions is true.
		- the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.

*/

let numB = 1;

if (numA > 0) {
	console.log("Hello");
} else if (numB > 0) {
	console.log("World");
}


// Another example:
city = "Tokyo"

if(city === "New York") {
	console.log("welcome to New York City");
} else if (city === "Tokyo") {
	console.log("welcome to Tokyo!");
}


/*
	else statement
		- executes a statement if all of our condistions are false 

*/

if(numA > 0) {
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log("Again");
}

/*let	age = parseInt(prompt("Enter your age:"));

if(age <= 18) {
	console.log("Not allowed to drink");
} else {
	console.log("Matanda ka na, shot na!");
}*/

/*
	Mini Activty
*/


let height = 160

function minHeight (height) {
	if (height < 150) {
	console.log("Did not pass the minimum height requirement");
} else if (height > 150) {
	console.log("Passed the minimum height requirement");
}}

minHeight(149);


let message = 'No message';
console.log(message)

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return 'Not a typhoon'
	} else if (windSpeed <= 61){
		return	'Tropical depression detected'
	} else if ( windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected'
	} else if (windSpeed >= 89 && windSpeed <= 177) {
		return 'Severe tropical storm detected'
	} else {
		return 'Typhoon detected'
	}
}

message = determineTyphoonIntensity(70);
console.log(message)

// console.log(determineTyphoonIntensity(90))


if (message == 'Tropical storm detected') {
	console.warn(message);
}


// Truthy and Falsy
/*
	In JS. a trythy value is a value that is considered tru when encountered in a boolean context.

	Falsy values
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
	
*/


// Truthy examples
if (true) {
	console.log("Truthy")
}

if (1) {
	console.log("Truthy")
}

if ([]) {
	console.log("Truthy")
}

// Falsy examples

if (false) {
	console.log("Falsy")
}
if (0) {
	console.log("Falsy")
}
if (undefined) {
	console.log("Falsy")
}
if (null) {
	console.log("Falsy")
}


// Conditional (Ternary) Operator - for short codes

/*
	Ternary Operator takes in three operands
		1. condition
		2. expression to execute if the condition is true/truthy.
		3. expression to execute if the condition is false/falsy.

	Syntax:
		(condition) ? ifTrue_expression : ifFalse_expression

*/

// Singe Statement Execution

let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False"
console.log("Result of the Ternary Operator: " + ternaryResult) //true;


/*// Multiple Statement Execution

let name;

function isOfLegalAge() {
	name = 'John';
	return 'You are of the legal agae limit';
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit';
}

let age = parseInt(prompt("What is your age?"))
console.log(age)

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in Function: " + legalAge + ',' + name);*/



// Switch Statement

/*
	Can be used as an alternative to an if ... else statement where the data to be used in the condition is of an expected input

	Syntax:
		switch (expression) {
			case <value>:
				statement;
				break;
			default:
				statement;
				break;
		}
*/

/*let day = prompt("What day of the week is it today?").toLowerCase()

console.log(day);

switch (day) {
	case 'monday':
		console.log("The color of the day is red.");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
		break;
}	*/

// break; - is for ending the loop.
// switch - is not a very popular statement in coding


// Try-Catch-Finally Statement

/*
	- try-catch is commonly used for error handling.
	- will still function even if the statement is not complete
*/

function showInternsityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed))
	}
	catch (error) {
		console.log(typeof error)
		console.log(error)
		console.warn(error.message)
	}
	finally {
		alert("Intencity updates will show new alert!!")
	}
}

showInternsityAlert(56)







	


